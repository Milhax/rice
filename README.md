# Hack3900-rice

### tools used :
* [i3](https://i3wm.org/)
* [i3bgwin](https://github.com/quantum5/i3bgwin)
* [urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html)
* [feh](https://feh.finalrewind.org/)
* [cava](https://github.com/karlstav/cava)
* [discord](https://discord.com/)
* [openasar](https://openasar.dev/)
* [bdeditor](https://bdeditor.dev/)
* [MaterialDesign discord theme](https://github.com/TheCommieAxolotl/BetterDiscord-Stuff/tree/main/MaterialDesign)
* [dunst](https://dunst-project.org/)
* [fish](https://fishshell.com/)
* [kitty](https://sw.kovidgoyal.net/kitty/)
* [neofetch](https://github.com/dylanaraps/neofetch)
* [nvim](https://neovim.io/)
* [polybar](https://polybar.github.io/)
* [polybar-mpris](https://github.com/0jdxt/polybar-mpris)
* [rofi](https://github.com/davatorium/rofi)
* [picom (pijulius)](https://github.com/pijulius/picom)
* [starship](https://starship.rs/)
* [pillow](https://python-pillow.org/)

### assets :
* [Celeste](https://exok.com/games/celeste/)
* [Rivals of Aether](https://rivalsofaether.com/)

### usage
navigate to [Documents/rice](https://gitlab.com/Milhax/rice/-/tree/main/Documents/rice) and run `clone.sh` to clone i3bgwin as well as `make_100_backgrounds.fish` to generate 100 random background images
after that you can easily select a random background from the created folder when starting your window manager with feh
