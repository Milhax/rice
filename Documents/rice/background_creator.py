from PIL import Image, ImageDraw
from sys import argv
from math import pi, cos, sin
from random import randrange
BGCOLOR = "#8f4bc8"
WIDTH = 1920
HEIGHT = 1080
TRICOUNT = 20

DARKTRI = ("#00000022", "#00000000")
LIGHTTRI = ("#FFFFFF11", "#FFFFFF00")

MINSIZE = 300
MAXSIZE = 1000

def torad(angle: int) -> float:
    return (angle * pi / 180)

def mut(pos: tuple[int, int], distance: int, angle: int) -> tuple[int, int]:
    return (
            int(pos[0] + distance * cos(torad(angle))),
            int(pos[1] + distance * sin(torad(angle)))
            )

def trangle(pos: tuple[int, int], angle: int, colors: tuple[str, str], size: int):
    pos2 = mut(pos, size, angle)
    pos3 = mut(pos, size, angle + 60)
    draw.polygon((pos, pos2, pos3), fill=colors[0], width=5)

img = Image.new("RGB", (WIDTH, HEIGHT), BGCOLOR)
draw = ImageDraw.Draw(img, "RGBA")

for i in range(TRICOUNT):
    tri = LIGHTTRI
    if randrange(0, 2):
        tri = DARKTRI
    trangle((randrange(WIDTH), randrange(HEIGHT)), randrange(360), tri, randrange(MINSIZE, MAXSIZE))

if (len(argv) <= 1):
    img.show()
else:
    file :str = argv[1]
    if not file.endswith(".png"):
        file += ".png"
    img.save(file)
    print("saved to : " + file)
