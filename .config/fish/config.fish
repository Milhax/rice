if status is-interactive
    # Commands to run in interactive sessions can go here

    alias clear="printf '\033[2J\033[3J\033[1;1H'"

    termprint ~/Documents/rice/PIXELS
    starship init fish | source
    source ~/.config/fish/functions/bettercatls.fish

	alias bgenerate 'feh --no-fehbg --bg-fill --randomize ~/Documents/rice/backgrounds/'
end
