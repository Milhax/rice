function mkin --argument-names path --description "creates a path and changes directory to it"
	if test -z $path
		echo "mkin takes one argument"
		return 1
	end
	if test "$path" != "$argv"
		echo too many args
	end
	mkdir -p $path
	cd $path
end
