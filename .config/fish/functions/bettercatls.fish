function betterls --wraps exa --description "exa with custom defaults"
	exa -l --icons --group-directories-first --no-filesize --no-time --no-user --color=always $argv
end

function bettercat --wraps bat --description "bat with custom defaults"
	bat $argv --paging=never
end

function betterccat --wraps bat --description "bat with custom defaults"
	bat $argv -pp
end



function ls --wraps exa --description "modded exa"
	if test -z "$argv"
		betterls
	end
	for i in $argv;
		if test -d $i
			betterls $i
		else
			bettercat $i
		end
	end
end

function la --wraps betterls --description "ls with hidden files"
	betterls -a $argv
end

function ll --wraps betterls --description "ls with 3 folders level recursion"
	betterls -TL3 $argv
end



function cat --wraps bat --description "modded bat"
	if test -z "$argv"
		bettercat
	end
	for i in $argv;
		if test -f $i
			bettercat $i
		else
			betterls $i
		end
	end
end

function ccat --wraps bat --description "modded bat"
	if test -z "$argv"
		betterccat
	end
	for i in $argv;
		if test -f $i
			betterccat $i
		else
			betterls $i
		end
	end
end
