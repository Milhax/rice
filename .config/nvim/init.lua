--general
vim.opt.nu = true
vim.opt.rnu = true
vim.opt.signcolumn = 'yes'
vim.opt.mouse = 'a'
vim.opt.termguicolors = true
vim.opt.ruler = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.cc = '80'
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.linebreak = true
vim.opt.expandtab = false
vim.g.mapleader = ' '

--neovide
if vim.fn.exists('g:neovide') then
	vim.g.neovide_refresh_rate = 60
	vim.g.neovide_refresh_rate_idle = 3
	vim.g.neovide_cursor_vfx_mode = "pixiedust"
	vim.g.neovide_cursor_vfx_particle_density=100.0
	vim.g.neovide_scroll_animation_length = 0.5
end

--eww
vim.cmd([[
colorscheme tokyodark
highlight MyGroup gui=bold
match MyGroup /./
]])

--normal keymaps
vim.keymap.set('n', '<C-s>', ":w<CR>")
vim.keymap.set('i', '<C-s>', "<Esc>:w<CR>i")
vim.keymap.set('v', '<C-s>', "<Esc>:w<CR>v")

--plugins
require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  use 'tiagovla/tokyodark.nvim'
  use 'voldikss/vim-floaterm'
end)

--plugins keymap
vim.keymap.set({'n', 'v', 'i', 't'}, '<C-t>', function() vim.cmd("FloatermToggle") end);
vim.keymap.set('n', '<leader>e', ":FloatermNew fzf<CR>");
vim.keymap.set('n', '<leader>s', ":FloatermNew --width=0.8 --height=0.8 rg ");
